## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1609743540423 release-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2185
- Brand: realme
- Flavor: cipher_RMX2185-userdebug
- Release Version: 12
- Id: SP2A.220305.012
- Incremental: eng.sinha_.20220309.040843
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX2185/RMX2185:11/RP1A.200720.011/1641984760095:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1609743540423-release-keys
- Repo: realme_rmx2185_dump_9541


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
